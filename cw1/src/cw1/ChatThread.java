package cw1;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ChatThread extends Thread {
    private DatagramSocket socket;

    public ChatThread(DatagramSocket socket) {
        this.socket = socket;
    }
    
    public void run() {
        try {
            
            while (true) {
                // DatagramSocket aSocket = new DatagramSocket();
                byte[] buffer = new byte[1024];
		        DatagramPacket reply = new DatagramPacket(buffer , buffer.length );
		        socket.receive(reply);
		        System.out.println( " Message : " + new String(reply.getData()));
            }

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }

}
