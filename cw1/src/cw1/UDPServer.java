package cw1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UDPServer {
	private HashMap<String,User> users = new HashMap<String,User>();
	public static void main ( String [] args ) {
		UDPServer server = new UDPServer();
		try {
		 	// args contain message content and server hostname
		 	DatagramSocket aSocket = new DatagramSocket(9876);
		 	byte[] buffer = new byte[1024];
		 	System.out.println("Server started!");
		 	while (true) {
		 		DatagramPacket request = new DatagramPacket(buffer, buffer.length);
		 		aSocket.receive(request);
				buffer = new byte[1024];

				String command = new String(request.getData(), StandardCharsets.UTF_8).trim();
				String code = command.split("[|]")[0];
				
				switch (code) {
					case "+":
						DatagramPacket registerReply = server.registerUser(request);
						aSocket.send(registerReply);
						break;
					case "-":
						DatagramPacket removeReply = server.removeUser(request);
						aSocket.send(removeReply);
						break;
					case "?":
						DatagramPacket filterReply = server.filterUsers(request);
						aSocket.send(filterReply);
						break;
					case "!":
					    //0 - potwierdzenie dla nadawcy, 1 - wiadomosc dla odbiorcy
						DatagramPacket[] replies = server.sendMessage(request);
						aSocket.send(replies[0]);
						if (!replies[1].equals(null)) aSocket.send(replies[1]);
						break;
					default:
						byte[] msg = (code + " is not a valid operator").getBytes();
						aSocket.send(new DatagramPacket(msg, msg.length, request.getAddress(), request.getPort()));
						break;
				}
		 	}
		} catch ( SocketException ex ) {
		 	Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex );
		} catch ( IOException ex ) {
		 	Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex );
		}
	}

	private DatagramPacket registerUser(DatagramPacket request) {
		String command = new String(request.getData(), StandardCharsets.UTF_8).trim();
		String name = command.split("[|]")[1];

		if(this.users.containsKey(name)) {
			byte[] msg = ("User with name " + name + " already exist").getBytes();
			return new DatagramPacket(msg, msg.length, request.getAddress(), request.getPort());
		}

		this.users.put(name, new User(name, request.getAddress(), request.getPort()));

		byte[] msg = ("+ | " + name + " | OK").getBytes();
		return new DatagramPacket(msg, msg.length, request.getAddress(), request.getPort());
	}

	private DatagramPacket removeUser(DatagramPacket request) {
		String command = new String(request.getData(), StandardCharsets.UTF_8).trim();
		String name = command.split("[|]")[1];

		if (!this.users.containsKey(name)) {
			byte[] msg = ("User with name " + name + " doesn't exist").getBytes();
			return new DatagramPacket(msg, msg.length, request.getAddress(), request.getPort());
		} 

		this.users.remove(name);
		byte[] msg = ("- | " + name + " | OK").getBytes();
		return new DatagramPacket(msg, msg.length, request.getAddress(), request.getPort());
	}

	private DatagramPacket filterUsers(DatagramPacket request) {
		String command = new String(request.getData(), StandardCharsets.UTF_8).trim();
		List<String> filteredUsers = new ArrayList<String>();

		try {
			String regex = command.split("[|]")[1];
			this.users.forEach((k,v) -> {
				if (k.contains(regex)) filteredUsers.add(k);
			});
		} catch (Exception ex) {
			filteredUsers.addAll(this.users.keySet());
		}
		
		byte[] msg = (String.join(",", filteredUsers)).getBytes();
		return new DatagramPacket(msg, msg.length, request.getAddress(), request.getPort());
	}

	private DatagramPacket[] sendMessage(DatagramPacket request) {
		String command = new String(request.getData(), StandardCharsets.UTF_8).trim();
		String name = command.split("[|]")[1];
		String msgToSend = command.split("[|]")[2];

		DatagramPacket confirmation;
		DatagramPacket message;

		if (!this.users.containsKey(name)) {
			byte[] msg = ("User with name " + name + "doesn't exist").getBytes();
			confirmation = new DatagramPacket(msg, msg.length, request.getAddress(), request.getPort());
			message = null;
		} else {
			User recipient = this.users.get(name);
			byte[] msg = ("! | " + name + " | OK").getBytes();
			confirmation = new DatagramPacket(msg, msg.length, request.getAddress(), request.getPort());
			message = new DatagramPacket(msgToSend.getBytes(), msgToSend.getBytes().length, recipient.ip, recipient.port);
		}

		DatagramPacket[] output = {confirmation, message};
		return output;
	}
}
