package cw1;

import java.net.InetAddress;

public class User {
    public String name;
    public InetAddress ip;
    public int port;

    public User(String name, InetAddress ip, int port) {
        this.name = name;
        this.ip = ip;
        this.port = port;
    }
}
