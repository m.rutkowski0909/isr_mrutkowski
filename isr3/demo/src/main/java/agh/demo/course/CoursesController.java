package agh.demo.course;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController()
public class CoursesController {
	
	@Autowired()
	private CourseService service;
	
	@RequestMapping("/courses")
	public List<Course> getCourses() {
		return service.getCourses();
	}
	
	@RequestMapping("/courses/{id}")
	public Course getCourse(@PathVariable String id) {
		try {
			return service.getCourse(id);
		} catch (NoSuchElementException ex) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping(value = "/courses")
	public void addCourse (@RequestBody Course course) {
		boolean result = service.addCourse(course);
		
		if (result)
			throw new ResponseStatusException(HttpStatus.CREATED);
		else 
			throw new ResponseStatusException(HttpStatus.CONFLICT);
	}
	
	@DeleteMapping(value = "/courses/{id}")
	public void deleteCourse(@PathVariable String id) {
		boolean result = service.deleteCourse(id);
		
		if (result) 
			throw new ResponseStatusException(HttpStatus.OK);
		else 
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(value = "/courses/{id}")
	public void editCourse(@PathVariable String id, @RequestBody Course editedCourse) {
		boolean result = service.editCourse(id, editedCourse);
		
		if (result)
			throw new ResponseStatusException(HttpStatus.OK);
		else 
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}
}
