package agh.demo.course;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.stereotype.Service;

@Service()
public class CourseService {
	private List<Course> courses = new ArrayList(Arrays.asList(
			new Course("java", "Java", "Java course"),
			new Course("javascript", "JavaScript", "JavaScript course"),
			new Course("angular", "Angular", "Angular framework course")
			));
	
	public List<Course> getCourses() {
		return this.courses;
	}
	
	public Course getCourse(String id) {
		return this.courses.stream().filter(c -> c.getId().equals(id)).findFirst().get();
	}
	
	public boolean addCourse(Course course) {
		try {
			getCourse(course.getId());
		} catch(NoSuchElementException ex) {
			this.courses.add(course);
			return true;
		}
		
		return false;
	}
	
	public boolean deleteCourse(String id) {
		try {
			getCourse(id);
		} catch(NoSuchElementException ex) {
			return false;
		}
		
		this.courses = this.courses.stream().filter(c -> !c.getId().equals(id)).toList();
		return true;
	}
	
	public boolean editCourse(String id, Course editedCourse) {
		Course courseToEdit;
		try {
			courseToEdit = getCourse(id);
		} catch (NoSuchElementException ex) {
			return false;
		}
		
		courseToEdit.setId(editedCourse.getId());
		courseToEdit.setName(editedCourse.getName());
		courseToEdit.setDescription(editedCourse.getDescription());
		courseToEdit.setLessons(editedCourse.getLessons());
		
		return true;
	}
}

