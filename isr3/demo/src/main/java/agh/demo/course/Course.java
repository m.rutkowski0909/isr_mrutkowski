package agh.demo.course;

import java.util.List;

public class Course {
	private String id;
	private String name;
	private String description;
	private List<String> lessons;
	
	public Course(String id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public Course() {}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getLessons() {
		return lessons;
	}

	public void setLessons(List<String> lessons) {
		this.lessons = lessons;
	}
	
	
}
