package agh.demo;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.springframework.http.MediaType;

import agh.demo.course.Course;

class TestClient {
	private final String host = "http://localhost";
	private final short port = 8080;
	private final String coursesUrl = "courses";
	
	private final Client client = ClientBuilder.newClient();
	private final WebTarget baseTarget = client.target(host + ":" + port);
	private final WebTarget coursesTarget = baseTarget.path(coursesUrl);
	private final WebTarget singleCourseTarget = coursesTarget.path("{id}");
	
	public static void main (String[] args) {
		TestClient testClient = new TestClient();
		
		//get all
		Response response = testClient.get();
		System.out.println("GET ALL");
		System.out.println(response);
		List<Course> courses = (List<Course>) response.readEntity(Course.class);
		System.out.println(courses);
		
		//get with id
		response = testClient.get("java");
		System.out.println("GET WITH ID");
		System.out.println(response);
		Course course = (Course) response.readEntity(Course.class);
		System.out.println(course);
		
		//put
		Course newCourse = new Course("react", "React", "JS library to build UI");
		response = testClient.put(newCourse);
		System.out.println("PUT");
		System.out.println(response);
		
		//post
		Course updateCourse = new Course("javascript", "JavaScript", "Basic JS course");
		response = testClient.post(updateCourse.getId(), updateCourse);
		System.out.println("POST");
		System.out.println(response);
		
		//delete
		String idToDelete = "java";
		response = testClient.delete(idToDelete);
		System.out.println("DELETE");
		System.out.println(response);
		
	}
	
	private Response get() {
		Invocation.Builder invocationBuilder = coursesTarget.request();
		return invocationBuilder.get();
	}
	
	private Response get(String id) {
		WebTarget target = singleCourseTarget.resolveTemplate("id", id);
		Invocation.Builder invocationBuilder = target.request();
		return invocationBuilder.get();
	}
	
	private Response put(Course course) {
		return coursesTarget.request().put(Entity.json(course));
	}
	
	private Response post(String id, Course course) {
		return coursesTarget.resolveTemplate("id", id).request().post(Entity.json(course));
	}
	
	private Response delete(String id) {
		return coursesTarget.resolveTemplate("id", id).request().delete();
	}
}

