package cw2;

import java.io.Console;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Vector;

public class MonitorClient {
    private MonitorInterface remoteObject;
    private String login;
    Console cnsl = System.console();
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Usage: MonitorClient <server host name> <login>");
            System.exit(-1);
        }

        MonitorClient client = new MonitorClient();
        client.login = args[1];

        Registry registry;
        
        try {
            registry = LocateRegistry.getRegistry(args[0]);
            client.remoteObject = (MonitorInterface) registry.lookup("MonitorServer");

            System.out.println("Logged as " + client.login);

            while(true) {
                System.out.println();
                System.out.println("--- MENU ---");
                System.out.println("1: Register");
                System.out.println("2: Deregister");
                System.out.println("3: Send message");
                System.out.println("4: Get registered users");
                System.out.println("5: Get messages");
                System.out.println("6: Get messages filtered by user");
                System.out.println("7: Get messages filtered by date");
                System.out.println("0: Exit");

                String code = client.cnsl.readLine("Select an option: ");

                switch (code) {
                    case "1":
                        client.register();
                        break;
                    case "2":
                        client.deregister();
                        break;
                    case "3":
                        client.sendMessage();
                        break;
                    case "4":
                        client.getUsers();
                        break;
                    case "5":
                        client.getMessages();
                        break;
                    case "6":
                        client.getMessagesByUser();
                        break;
                    case "7":
                        client.getMessagesByDate();
                        break;
                    case "0":
                        System.exit(0);
                    default:
                        System.out.println("Enter a valid option!");
                        break;
                }

                Thread.sleep(500);
                System.out.println();
            }
            
        } catch (RemoteException ex) {
            ex.printStackTrace();
        } catch (NotBoundException ex) {
            ex.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private boolean needToRegister() throws RemoteException {
        if (remoteObject.isUserRegistered(this.login)) {
            return false;
        } else {
            System.out.println("You need to be registered to use that option");
            return true;
        }
    }

    private void register() throws RemoteException {
        if (remoteObject.isUserRegistered(this.login)) {
            System.out.println("User with login " + this.login + " is already registered. Please select another login.");
            System.exit(-1);
        } else {
            remoteObject.register(this.login);
            System.out.println("Registered successfully as " + this.login);
        }
        
    }

    private void deregister() throws RemoteException {
        if (needToRegister()) return;

        remoteObject.deregister(this.login);
        System.out.println("Registered successfully");
    }

    private void sendMessage() throws RemoteException {
        if (needToRegister()) return;
        
        String msg = cnsl.readLine("Enter your message: ");
        remoteObject.sendMessage(msg, this.login);
    }

    private void getUsers() throws RemoteException {
        if (needToRegister()) return;

        Vector<String> users = remoteObject.getRegisteredUsers();
        System.out.println("There are " + users.size() + " registered users");
        for (int i = 0; i < users.size(); i++) {
            System.out.print(users.get(i) + " ");
        }
        System.out.println();
    }

    private void getMessages() throws RemoteException {
        if (needToRegister()) return;

        Vector<Message> messages = remoteObject.getAllMessages();
        for (int i = 0; i < messages.size(); i++) {
            System.out.println(messages.get(i).getFormatedMessage());
        }
    }

    private void getMessagesByUser() throws RemoteException {
        if (needToRegister()) return;

        String user = cnsl.readLine("Enter name for filtering: ");
        Vector<Message> messages = remoteObject.getMessagesByUser(user);
        System.out.println("There are " + messages.size() + " messages from " + user);
        for (int i = 0; i < messages.size(); i++) {
            System.out.println(messages.get(i).getFormatedMessage());
        }
    }

    private void getMessagesByDate() throws RemoteException {
        if (needToRegister()) return;

        String date = cnsl.readLine("Enter date for filtering (yyyy-MM-dd HH:mm:ss): ");
        Vector<Message> messages = remoteObject.getMessagesByDate(date);
        System.out.println("There are " + messages.size() + "messages from " + date);
        for (int i = 0; i < messages.size(); i++) {
            System.out.println(messages.get(i).getFormatedMessage());
        }
    }
}