package cw2;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Vector;

public class MonitorServant extends UnicastRemoteObject implements MonitorInterface{
    private Vector<String> users;
    private Vector<Message> messages;

    protected MonitorServant() throws RemoteException {
        users = new Vector<String>();
        messages = new Vector<Message>();
    }

    //registration
    public boolean register(String name) throws RemoteException {
        System.out.println("** New user " + name + " registered **");
        return this.users.add(name);
    }

    public boolean deregister(String name) throws RemoteException {
        System.out.println("** User " + name + " was deregistered **");
        return this.users.remove(name);
    }

    public boolean isUserRegistered(String name) throws RemoteException {
        return users.contains(name);
    }

    //sending messages
    public void sendMessage(String msg, String user) throws RemoteException {
        Message newMsg = new Message(user, msg);
        this.messages.add(newMsg);
        System.out.println(newMsg.getFormatedMessage());
    }

    //users
    public Vector<String> getRegisteredUsers() {
        return this.users;
    }

    //messages
    public Vector<Message> getAllMessages() {
        return this.messages;
    }

    public Vector<Message> getMessagesByUser(String user) {
        Vector<Message> filteredMessages = new Vector<Message>();

        for (int i = 0; i < this.messages.size(); i++) {
            if (this.messages.get(i).getUser().equals(user)) {
                filteredMessages.add(this.messages.get(i));
            }
        }

        return filteredMessages;
    }

    public Vector<Message> getMessagesByDate(String date) {
        Vector<Message> filteredMessages = new Vector<Message>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime convertedDate = LocalDateTime.parse(date, formatter);

        for (int i = 0; i < this.messages.size(); i++) {
            if (this.messages.get(i).getDate().isAfter(convertedDate)) {
                filteredMessages.add(this.messages.get(i));
            }
        }

        return filteredMessages;
    }
}
