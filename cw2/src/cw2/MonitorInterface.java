package cw2;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;

public interface MonitorInterface extends Remote{
    boolean register(String name) throws RemoteException;
    boolean isUserRegistered(String name) throws RemoteException;
    boolean deregister(String name) throws RemoteException;

    void sendMessage(String msg, String user) throws RemoteException;

    Vector<String> getRegisteredUsers() throws RemoteException;

    Vector<Message> getAllMessages() throws RemoteException;
    Vector<Message> getMessagesByUser(String user) throws RemoteException;
    Vector<Message> getMessagesByDate(String date) throws RemoteException;
}
