package cw2;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class MonitorServer {
    Registry registry;
    MonitorServant servant;

    public static void main (String[] args) {
        try {
            MonitorServer server = new MonitorServer();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }

    protected MonitorServer() throws RemoteException {
        try {
            registry = LocateRegistry.createRegistry(1099);
            servant = new MonitorServant();
            registry.rebind("MonitorServer", servant);
            System.out.println("Server ready");
        } catch (RemoteException ex) {
            ex.printStackTrace();
            throw ex;
        }
    }
        
}
