package cw2;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class Message implements Serializable {
    private String user;
    private String text;
    private LocalDateTime date;

    public Message(String user, String text) {
        this.user = user;
        this.text = text;
        this.date = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
    }

    public String getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getFormatedMessage() {
        return date + " " + user + ": " + text;
    }
}
